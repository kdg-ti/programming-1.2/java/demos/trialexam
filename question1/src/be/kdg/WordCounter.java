package be.kdg;

import java.io.*;
import java.util.*;

public class WordCounter {

  // TODO: initialise wordCounts
  private Map<String,Integer> wordCounts = new HashMap<>() ;

  public Map<String, Integer> getWordCounts() {
    return wordCounts;
  }

  public  void countWordsInFile(String fileName) throws IOException{
    // TODO add number of times each word appears in file with name fielName to wordCounts
    //  any exception is wrapped in an IOException

  }

  public void countWordsInString(String line) {
    // TODO add number of times each word appears in line to wordCounts
    //  words are in alphabetical order
    String[] words = line.split(" ");
    for(String word : words){
      String loweredWord = word.toLowerCase();
      Integer count = wordCounts.get(loweredWord);
      if (count == null){
        wordCounts.put(loweredWord, 1);
      } else {
        wordCounts.put(loweredWord, count +1);
      }
    }
  }




  public Map<Integer,List<String>> wordsByCount(Map<String,Integer> countByWords) {
    // TODO replace the statement below, returning a map, with for each frequency a list of words that appear that many times
    //    lower frequencies come first
    Map <Integer,List<String>> countByFrequency = new HashMap<>();
    for(String word : countByWords.keySet()){
      int frequency = countByWords.get(word);
      List<String> present = countByFrequency.get(frequency);
      if (present != null){
        present.add(word);
      } else {
        present = new ArrayList<>();
        present.add(word);
      }
      countByFrequency.put(frequency,present);
    }
    return countByFrequency;
  }
}
