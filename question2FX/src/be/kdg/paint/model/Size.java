package be.kdg.paint.model;

public enum Size {
	SMALL(10),
	MEDIUM(40),
	LARGE(70);

	int pixels;
	Size(int pixels) {
		this.pixels = pixels;
	}

	public int getPixels() {
		return pixels;
	}
}
