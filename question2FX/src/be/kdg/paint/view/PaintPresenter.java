package be.kdg.paint.view;

import be.kdg.paint.model.*;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;

public class PaintPresenter {
	private final Painting model;
	private final PaintView view;

	public PaintPresenter(Painting model, PaintView view) {
		this.model = model;
		this.view = view;
		addEventHandlers();
		updateView();
	}

	private void addEventHandlers() {
		view.getCanvas().setOnMouseClicked(new DrawHandler());
		view.getUndo().setOnAction(e -> {
			model.getComponents().remove(model.getComponents().size()-1);
			updateView();
		});

	}

	private void updateView() {
		view.clear();
		// TODO fill the view with model data
		for(Component component : model.getComponents()){
			GraphicsContext graphics = view.getCanvas().getGraphicsContext2D();
			graphics.setStroke(component.getColor());
			double fromX = component.getX();
			double fromY = component.getY();
			double size = component.getSize();
			switch(component.getShape()){
				case "Line" -> graphics.strokeLine(fromX,fromY,fromX + size,fromY + size);
				case "Square" -> graphics.strokeRect(fromX,fromY,size,size);
				case "Circle" -> graphics.strokeOval(fromX,fromY,size,size);
				default -> graphics.strokeText("Shape not found",fromX,fromY);
			}
		}
	}


	private class DrawHandler implements EventHandler<MouseEvent> {
		@Override
		public void handle(MouseEvent event) {
//			model.getComponents().add(new Component(event.getX(),
//				event.getY(),
//				view.getShapePicker().getValue(),
//				view.getColorPicker().getValue()));
			model.getComponents().add(new Component(event.getX(),
				event.getY(),
				view.getShapePicker().getValue(),
				view.getColorPicker().getValue(),
				Size.valueOf(view.getSizePicker().getValue()).getPixels()));
			updateView();
		}
	}
}
